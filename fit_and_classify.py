from skimage.color import rgb2gray
from scipy import ndimage
import numpy as np
from skimage.transform import resize
from sklearn.svm import SVC
from os import listdir
from os.path import isfile, join

def extract_hog(img):
    eps = 0.01
    img_size = 45
    img_resized = resize(img, (img_size, img_size))
    img_gray = rgb2gray(img_resized)

    d_x = np.array([[-1, 0, 1]])
    d_y = np.array([[-1], [0], [1]])
    I_x = ndimage.convolve(img_gray, d_x)
    I_y = ndimage.convolve(img_gray, d_y)
    G_mod = np.sqrt(I_x**2 + I_y**2)
    teta = np.arctan2(I_x, I_y)

    cellRows = cellCols = 5
    binCount = 11

    blockColCells = blockRowCells = 3
    pi = 3.15
    bins = np.linspace(-pi, pi, binCount + 1)

    blocks = [[np.array([])] * int(img_size / (cellCols * blockColCells))] * int(img_size / (cellRows * blockRowCells))
    for i in range(int(img_size / cellRows)): #!!!!
        for j in range(int(img_size / cellCols)):
            cell = teta[i * cellRows:(i + 1)* cellRows, j * cellCols:(j + 1) * cellCols]
            weights = G_mod[i * cellRows:(i + 1)* cellRows, j * cellCols:(j + 1) * cellCols]
            res_hist = np.histogram(cell.reshape((1,-1)), bins = bins, weights = weights.reshape((1,-1)))
            blocks[int(i / blockRowCells)][int(j / blockColCells)] = np.hstack((blocks[int(i / blockRowCells)][int(j / blockColCells)], res_hist[0]))

    descriptor = np.array([])
    for i in range(len(blocks)):
        for j in range(len(blocks[0])):
            descriptor = np.hstack((descriptor, blocks[i][j] / np.sqrt(np.sqrt(blocks[i][j] ** 2) + eps)))

    return descriptor

def fit_and_classify(train_features, train_labels, test_features):
    parameters = {'gamma': 'auto', 'kernel': 'linear', 'C': 0.005, 'degree': 3}
    svc = SVC(**parameters)
    svc.fit(train_features, train_labels)
    return svc.predict(test_features)

def FitModel(data_dir):
    import pickle
    from glob import glob
    from numpy import zeros
    from os.path import basename, join
    from skimage.io import imread

    train_dir = join(data_dir, 'train')
    test_dir = join(data_dir, 'test')

    def read_gt(gt_dir):
        fgt = open(join(gt_dir, 'gt.csv'))
        next(fgt)
        lines = fgt.readlines()

        filenames = []
        labels = zeros(len(lines))
        for i, line in enumerate(lines):
            filename, label = line.rstrip('\n').split(',')
            filenames.append(filename)
            labels[i] = int(label)

        return filenames, labels

    def extract_features(path, filenames):
        hog_length = len(extract_hog(imread(join(path, filenames[0]))))
        data = zeros((len(filenames), hog_length))
        for i in range(0, len(filenames)):
            filename = join(path, filenames[i])
            data[i, :] = extract_hog(imread(filename))
        return data

    train_filenames, train_labels = read_gt(train_dir)

    train_features = extract_features(train_dir, train_filenames)
    pickle.dump(train_features, open("train_features.p", "wb"))
    pickle.dump(train_labels, open("train_labels.p", "wb"))

    train_features = pickle.load(open("train_features.p", "rb"))
    train_labels = pickle.load(open("train_labels.p", "rb"))

    from sklearn.model_selection import GridSearchCV
    #'rbf' 'linear'

    parameters = {'kernel': ('linear', ), 'C': np.linspace(0.001, 10, 20), 'degree': [1, 2, 3, 4]}
    svc = SVC(gamma='auto')
    clf = GridSearchCV(svc, parameters, cv=2, n_jobs=4, verbose=3, iid = True)
    clf.fit(train_features, train_labels)

    print(sorted(clf.cv_results_.keys()))
    
    #dump fitted model
    
    # didicated function predict(fitted_model, test_dir) 
    test_filenames = [f for f in listdir(test_dir) if isfile(join(test_dir, f))]
    test_features = extract_features(test_dir, test_filenames)
    print(test_filenames)
    print(clf.predict(test_features))

FitModel('/home/ryhor/SHAD/road_sign_shape/public_data/00_input')
